const url = 'https://www.googleapis.com/books/v1/volumes?q=HTML5';
fetch(url)
	.then(response => response.json())
	.then(data => {
		localStorage.setItem('books', JSON.stringify(data));
		data.items.forEach((info, i) => {
			const book = createBook(info);
			if (i === 8 || i === 9) {
				document.getElementById('featured').appendChild(book);
			} else {
				document.getElementById('bookList').appendChild(book);
			}
		});
	})
	.catch(err => {
		console.log(err);
	});

// If subtitle is undefined, return empty string
function defined (subtitle) {
  if (subtitle === 'undefined') {
    return '';
  } else {
    return subtitle;
  }
}

function createBook(info) {
	// creates a new book div
	const book = document.createElement('Div');
	book.className = 'book';
	let booksSelected = JSON.parse(localStorage.getItem('selectedBooks'));

	let checkBooks = () => {
		let selectBook = booksSelected[info.id]
		if (selectBook) {
			book.classList.add('is-selected');
		} else {
			book.classList.remove('is-selected');
		}
	};

	checkBooks();
	// selects a book by highlighting it
	book.onclick = function () {
		let selectedBooks = booksSelected ? booksSelected : {}
		if (selectedBooks[info.id]) {
			selectedBooks[info.id] = !selectedBooks[info.id]
		} else {
			selectedBooks[info.id] = true;
		}
		localStorage.setItem('selectedBooks', JSON.stringify(selectedBooks));
		checkBooks();
	};

	// creates a div to put the image
	const imgDiv = document.createElement('Div');
	const imgCover = document.createElement('Img');
	imgCover.src = info.volumeInfo.imageLinks.smallThumbnail;
	imgDiv.className = 'cover';
	imgDiv.appendChild(imgCover);

	// creates a div to put the book information
	let divInfo = document.createElement('Div');
	let shortenedDescription = `${info.volumeInfo.description}`.substring(0, 140);
	divInfo.innerHTML = `${info.volumeInfo.title}
    ${defined(info.volumeInfo.subtitle)} <br>
    ${info.volumeInfo.authors} <br>
    Page Count: ${info.volumeInfo.pageCount} <br>` +
		shortenedDescription + '...' + `<br>
    <br><br>`;

	console.log(`${info.volumeInfo.subtitle}`);
	console.log(`${defined(info.volumeInfo.subtitle)}`);
	divInfo.className = 'bookInformation';
	book.appendChild(imgDiv);
	book.appendChild(divInfo);
	return book;
}